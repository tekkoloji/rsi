from client import Client
from datetime import datetime
from datetime import timedelta
import time
import requests
import json
import talib as ta
import numpy as np


class RSI:
    def __init__(self, piyasa):
        self.piyasa = piyasa
        self.minimumRsi = 30
        self.client = None
        self.periyot = Client.KLINE_INTERVAL_1HOUR
        self.tabloAdi = "rsi"
        self.limit = 50
        self.durum = 50.0
        self.gSheet = "https://script.google.com/macros/s/AKfycbzuNtb_XVjlVUvtesxrdRtHJkykyABnDUcrCb5Y_c2dcEWTMP7tuIHyM9JVE4w7PxB6/exec"
        self.baglan()

    def baglan(self):
        try:
            self.client = Client("", "")
        except:
            print("Binance'e bağlanılamadı.")
            time.sleep(3)

    def tara(self):
        while(True):
            try:
                mumlar = self.client.get_klines(
                    symbol=self.piyasa, interval=self.periyot, limit=self.limit)
                yeniDurum = self.rsiHesapla(mumlar, 14)

                print("+" if yeniDurum > self.minimumRsi else "-",
                      flush=True, end="")

                if self.durum < self.minimumRsi and yeniDurum > self.minimumRsi:
                    # RSI OverSell
                    print("AL", self.piyasa)
                    self.sinyal()
                    time.sleep(60*60)
                self.durum = yeniDurum

            except Exception as e:
                print("HATA :", e)
                time.sleep(3)
                self.baglan()

            time.sleep(10*60)

    def rsiHesapla(self, mumlar, mumSayisi):
        sonuc = []
        for mum in mumlar:
            sonuc.append(float(mum[4]))

        nDizi = np.array(sonuc)

        rsi = ta.RSI(nDizi, timeperiod=mumSayisi)[-1]
        return rsi

    def sinyal(self):
        zaman = datetime.utcnow() + timedelta(hours=3)
        zamanText = zaman.strftime("%Y.%m.%d %H:%M:%S")
        fiyat = float(self.client.get_symbol_ticker(
            symbol=self.piyasa)["price"])
        data = {"zaman": zamanText, "piyasa": self.piyasa, "fiyat": fiyat}
        veri = json.dumps(data)

        link = self.gSheet+"?action=insert&table="+self.tabloAdi+"&data="+veri
        while(True):
            try:
                requests.get(link)
                break
            except Exception as e:
                print("HATA :", e)
                self.baglan()

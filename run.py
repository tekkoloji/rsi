from rsi import RSI
from threading import Thread
import requests

listeUrl = "https://api.binance.com/api/v3/exchangeInfo"
semboller = requests.get(url=listeUrl).json()["symbols"]

liste = []

for sembol in semboller:
    sAdi = sembol["symbol"]
    sAktifMi = sembol["status"] == "TRADING"
    sSpotMu = "SPOT" in sembol["permissions"]

    if(sAktifMi and sSpotMu and sAdi[-4:]=="USDT"):
        liste.append(sAdi)

###
for sembol in liste:
    Thread(target=RSI(sembol).tara).start()
